import java.sql.Struct;

/**
 * Created by Fleps_000 on 10.04.2014.
 */
public class Studying extends Experience {
    Organizations studyingOrganizations;
    String specialityName;
    String duration;

    Studying (Organizations studyingOrganizations, String specialityName, String duration) {
        this.studyingOrganizations = studyingOrganizations;
        this.specialityName = specialityName;
        this.duration = duration;
    }

    public String getDuration() {
        return duration;
    }

    public Organizations getStudyingOrganizations() {
        return studyingOrganizations;
    }

    public String getSpecialityName() {
        return specialityName;
    }
}
