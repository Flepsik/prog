import java.util.ArrayList;

/**
 * Created by Fleps_000 on 10.04.2014.
 */
public class Working extends Experience {
    Organizations workingOrganizations;
    String duration;
    ArrayList<String> usedTechnologies;


    Working (Organizations workingOrganizations, String duration, ArrayList<String> usedTechnologies) {
        this.workingOrganizations = workingOrganizations;
        this.duration = duration;
        this.usedTechnologies = new ArrayList<String>(usedTechnologies);
    }


    public Organizations getWorkingOrganizations() {
        return workingOrganizations;
    }

    public String getDuration() {
        return duration;
    }

    public ArrayList<String> getUsedTechnologies() {
        return usedTechnologies;
    }
}
