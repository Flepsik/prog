/**
 * Created by Fleps_000 on 10.04.2014.
 */
public class Organizations {
    String organizationName;
    String phoneNumber;
    String address;
    String Email;

    Organizations () {

    }
    public String getOrganizationName() {
        return organizationName;
    }

    public String getAddress() {
        return address;
    }

    public String getEmail() {
        return Email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

}
