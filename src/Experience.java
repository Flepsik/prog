import java.util.ArrayList;

/**
 * Created by Fleps_000 on 10.04.2014.
 */
public abstract class Experience {
    ArrayList<Experience> experience;

    Experience(){
        experience = new ArrayList<Experience>();
    }
    public void addWorkingExperience (Organizations organizations, String duration, ArrayList<String> usedTechnologies) {
        experience.add(new Working(organizations, duration, usedTechnologies));
    }
    public void addStudyingExperience (Organizations organizations, String specialityName, String duration) {
        experience.add(new Studying(organizations, specialityName, duration));
    }
    public ArrayList<Experience> getExperience() {
        return experience;
    }
}
